//
//  StationsTableViewAssistant.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 22/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation
import UIKit

protocol StationTableProtocol: class{
    func stationSelected(code: String, name: String)
}

class StationsTableViewAssistant:NSObject, UITableViewDataSource, UITableViewDelegate {
    weak var delegate: StationTableProtocol?
    
    let viewModel: StationViewModel = {
        return StationViewModel()
    }()
    
    var tableView: UITableView!
    
    func updateTable(with text: String){
        viewModel.filterStations(with: text)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.stationSelected(code: viewModel.getStationCode(at: indexPath.row), name: viewModel.getStationName(at: indexPath.row))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.stationCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = viewModel.getStation(at: indexPath.row)
        cell.selectionStyle = .none
        return cell
    }
}

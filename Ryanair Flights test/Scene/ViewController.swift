//
//  ViewController.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 17/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import UIKit
import SnapKit

class ViewController: UIViewController {
    
    let viewModel: ViewModel = {
        return ViewModel()
    }()
    
    let stationTableAux = StationsTableViewAssistant()
    var activeTextField = UITextField()
    
    let datePicker = UIDatePicker()
    let adtPassengerNumber = Array(1...6)
    let chdPassengerNumber = Array(0...6)
    
    private lazy var originTextField: PaddedTxtField = {
        let textField = PaddedTxtField(placeholder:"Origin")
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.clearsOnBeginEditing = true
        textField.delegate = self
        textField.returnKeyType = .done
        return textField
    }()
    
    private lazy var destinationTextField: UITextField = {
        let textField = PaddedTxtField(placeholder: "Destination")
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.clearsOnBeginEditing = true
        textField.delegate = self
        textField.returnKeyType = .done
        return textField
    }()
    
    private lazy var departureTextField: UITextField = {
        let textField = PaddedTxtField(placeholder: "Date")
        textField.delegate = self
        textField.returnKeyType = .done
        return textField
    }()
    
    private lazy var adultsTextField: UITextField = {
        let textField = PaddedTxtField(placeholder:"Adults")
        textField.text = "1"
        textField.delegate = self
        return textField
    }()
    
    private lazy var childrenTextField: UITextField = {
        let textField = PaddedTxtField(placeholder:"Children")
        
        textField.text = "0"
        textField.delegate = self
        return textField
    }()
    
    private lazy var teenTextField: UITextField = {
        let textField = PaddedTxtField(placeholder: "Teens")
        
        textField.text = "0"
        textField.delegate = self
        return textField
    }()
    
    private lazy var searchButton: UIButton = {
        let button = UIButton(type: .roundedRect)
        button.setTitle("Search", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .orange
        button.layer.cornerRadius = 4
        button.addTarget(self, action: #selector(searchFlights), for: .touchUpInside)
        button.isEnabled = false
        button.alpha = 0.6
        return button
    }()
    
    private lazy var flightsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        
        tableView.register(FlightViewCell.self, forCellReuseIdentifier: "cellId")
        return tableView
    }()
    
    private lazy var stationsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = stationTableAux
        tableView.dataSource = stationTableAux
        tableView.showsVerticalScrollIndicator = false
        tableView.bounces = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.isHidden = true
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        stationTableAux.tableView = stationsTableView
        stationTableAux.delegate = self
        self.view.backgroundColor = .white
        setup()
        setupDatePicker()
        setupNumericPicker()
    }
    
    // MARK: - Private Functions
    private func setup() {
        view.addSubview(originTextField)
        view.addSubview(destinationTextField)
        view.addSubview(departureTextField)
        view.addSubview(adultsTextField)
        view.addSubview(childrenTextField)
        view.addSubview(teenTextField)
        view.addSubview(searchButton)
        view.addSubview(flightsTableView)
        view.addSubview(stationsTableView)
        
        originTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.height.equalTo(40).priority(250)
            make.top.equalTo(view).offset(60)
        }
        
        destinationTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.height.equalTo(40).priority(250)
            make.top.equalTo(originTextField.snp.bottom).offset(12)
        }
        
        departureTextField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.height.equalTo(40).priority(250)
            make.top.equalTo(destinationTextField.snp.bottom).offset(12)
        }
        
        adultsTextField.snp.makeConstraints { make in
            make.leading.equalTo(view).inset(24)
            make.height.equalTo(40).priority(250)
            make.width.equalTo(60).priority(250)
            make.top.equalTo(departureTextField.snp.bottom).offset(12)
        }
        
        teenTextField.snp.makeConstraints { make in
            make.leading.equalTo(adultsTextField.snp.trailing).offset(6)
            make.height.equalTo(40).priority(250)
            make.width.equalTo(60).priority(250)
            make.top.equalTo(departureTextField.snp.bottom).offset(12)
        }
        
        childrenTextField.snp.makeConstraints { make in
            make.leading.equalTo(teenTextField.snp.trailing).offset(6)
            make.height.equalTo(40).priority(250)
            make.width.equalTo(60).priority(250)
            make.top.equalTo(departureTextField.snp.bottom).offset(12)
        }
        
        searchButton.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.height.equalTo(40).priority(250)
            make.top.equalTo(adultsTextField.snp.bottom).offset(12)
        }
        
        flightsTableView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.top.equalTo(searchButton.snp.bottom).offset(12)
            make.bottom.equalTo(view).offset(24)
        }
        
        stationsTableView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view).inset(24)
            make.top.equalTo(originTextField.snp.bottom).offset(12)
            make.bottom.equalTo(view).offset(24)
        }
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(_:)))
//        self.view.addGestureRecognizer(tap)
    }
    
    func setupDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date

        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "select", style: .plain, target: self, action: #selector(doneDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        departureTextField.inputAccessoryView = toolbar
        departureTextField.inputView = datePicker
    }
    
    @objc func doneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        departureTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
        checkFields()
    }

    @objc func cancelDatePicker(){
        departureTextField.text = ""
        self.view.endEditing(true)
    }
    
    func setupNumericPicker(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "select", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.donePickerPressed))
        
        // removing the space element, the "done" button will be left aligned
        // we can add more items
        toolBar.setItems([space, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        toolBar.sizeToFit()
        
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
//        pickerView.backgroundColor = .white
        
        adultsTextField.inputView = pickerView
        adultsTextField.inputAccessoryView = toolBar
        
        childrenTextField.inputView = pickerView
        childrenTextField.inputAccessoryView = toolBar
        
        teenTextField.inputView = pickerView
        teenTextField.inputAccessoryView = toolBar
    }
    
    @objc func donePickerPressed(_ sender: Any){
        self.view.endEditing(true)
    }
    
    @objc private func dismissKeyboard(_ sender: UITapGestureRecognizer){
        view.endEditing(true)
        stationsTableView.isHidden = true
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        
        if textField == originTextField || textField == destinationTextField{
            stationTableAux.updateTable(with: textField.text ?? "")
            stationsTableView.isHidden = false
        
            self.stationsTableView.snp.remakeConstraints { (make) -> Void in
                make.leading.trailing.equalTo(view).inset(24)
                make.top.equalTo(textField.snp.bottom).offset(1)
                make.bottom.equalTo(view).offset(24)
            }
            return
        }
        stationsTableView.isHidden = true
    }
    
    func checkFields(){
        
        if viewModel.checkOrigin() && viewModel.checkDestination() && !(departureTextField.text?.isEmpty  ?? false){
            searchButton.isEnabled = true
            searchButton.alpha = 1
        }else{
            searchButton.isEnabled = false
            searchButton.alpha = 0.6
        }
    }
    
    @objc private func searchFlights() {
        viewModel.requestFlightAvailability(dateout: departureTextField.text ?? "",
                                            adt: adultsTextField.text ?? "1",
                                            teen: teenTextField.text ?? "0",
                                            chd:childrenTextField.text ?? "0")
    }
    
    private func showAlert(title:String, message:String, controller:UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        controller.present(alert, animated: true, completion: nil)
    }
}

extension ViewController: StationTableProtocol{
    func stationSelected(code: String, name: String) {
        view.endEditing(true)
        activeTextField.text = code + " " + name
        stationsTableView.isHidden = true
        if activeTextField == originTextField {
            viewModel.setOrigin(code: code)
        }
        else if activeTextField == destinationTextField{
            viewModel.setDestination(code: code)
        }
        checkFields()
    }
}

extension ViewController: RequestProtocol{
    func response(_ success: Bool) {
        flightsTableView.reloadData()
        if !success{
            showAlert(title: "Warning!", message: "Request failed!!", controller: self)
        }
    }
    
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        if textField == originTextField{
            viewModel.clearOrigin()
        }
        if textField == destinationTextField{
            viewModel.clearDestination()
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == originTextField{
            showAlert(title: "", message: "Please select a valid origin", controller: self)
            return false
        }
        if textField == destinationTextField{
            showAlert(title: "", message: "Please select a valid destination", controller: self)
            return false
        }
        
        view.endEditing(true)
        stationsTableView.isHidden = true
        checkFields()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("end")
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getFlightCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as! FlightViewCell
        cell.dateLabel.text = viewModel.getFlightDate(for: indexPath.row)
        cell.flightLabel.text = viewModel.getFlightCode(for: indexPath.row)
        cell.valueLabel.text = viewModel.getFlightRegularFare(for: indexPath.row)
        return cell
    }
}

extension ViewController: UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if activeTextField == adultsTextField{
        return adtPassengerNumber.count
        }else{
            return chdPassengerNumber.count
        }
    }
}

extension ViewController: UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var number = chdPassengerNumber[row]
        if activeTextField == adultsTextField{
            number = adtPassengerNumber[row]
        }
        return number.description
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if activeTextField == adultsTextField{
            activeTextField.text = adtPassengerNumber[row].description
        }else{
            activeTextField.text = chdPassengerNumber[row].description
        }
    }
}


//
//  StationViewModel.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation

class StationViewModel: NSObject{
    weak var delegate: RequestProtocol?
    var allStations = [Station]()
    var filteredStations = [Station]()

    override init(){
        super.init()
        requestStations()
    }
    
    func requestStations(){
        ServiceManager.requestStations(callback: {(error, result, code) in
            guard error == nil else{
                self.delegate?.response(false)
                return
            }
            
            self.allStations.removeAll()
            guard let list = result as? [Station] else{
               guard let item = result as? Station else{
                   self.delegate?.response(true)
                   return
               }

               self.allStations.append(item)
               self.delegate?.response(true)
               return
            }
            self.allStations = list
            self.delegate?.response(true)
        })
    }
    
    func filterStations(with text: String){
        let filtered = allStations.filter({ $0.name!.localizedCaseInsensitiveContains(text) || $0.code!.localizedCaseInsensitiveContains(text)})
        filteredStations = filtered
    }
    
    func getStation(at index: Int) -> String{
        return filteredStations[index].code! + " " + filteredStations[index].name!
    }
    
    func getStationName(at index: Int) -> String{
        return filteredStations[index].name!
    }
    
    func getStationCode(at index: Int) -> String{
        return filteredStations[index].code!
    }
    
    func stationCount() -> Int{
        return filteredStations.count
    }
}

//
//  ViewModel.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation

struct FlightInfo{
    var date: String?
    var time: String?
    var code: String?
    var price: String?
}

protocol RequestProtocol:class{
    func response(_ success:Bool)
}

class ViewModel: NSObject{
    weak var delegate: RequestProtocol?
    var flights = [FlightInfo]()
    var currency = "EUR"
    var searchDetail:SearchDetail! = SearchDetail()
    
    func requestFlightAvailability(dateout: String, adt: String, teen: String, chd:String){
        searchDetail.dateout = dateout
        searchDetail.adt = adt
        searchDetail.teen = teen
        searchDetail.chd = chd
        ServiceManager.requestFlights(checkFor: searchDetail, callback: {(error, result, code) in
            
            self.flights.removeAll()
            
            guard error == nil else{
                self.delegate?.response(false)
                return
            }
            
            guard let availability = result as? Availability else{
                self.delegate?.response(true)
                return
            }
            
            self.currency = availability.currency ?? ""
            var trips = [Trip]()
            guard let list = availability.trips?.allObjects as? [Trip] else{
                guard let item = result as? Trip else{
                   self.delegate?.response(true)
                   return
                }

                trips.append(item)
                self.flights = self.getFlights(trips: trips)
                self.delegate?.response(true)
                return
            }
            
            self.flights = self.getFlights(trips: list)
            self.delegate?.response(true)
        })
    }
    
    func sortByDate(list: [FlightInfo], ascending: Bool = true) -> [FlightInfo]{
        return list.sorted(by: { (item1, item2) -> Bool in
            
            if ascending{
                if item1.date != item2.date { 
                    return item1.date!.compare(item2.date!) == ComparisonResult.orderedAscending
                }
                else {
                    return item1.time!.compare(item2.time!) == ComparisonResult.orderedAscending
                }
            }
            return item1.date!.compare(item2.date!) == ComparisonResult.orderedDescending
        })
    }
    
    func getFlights(trips: [Trip]) -> [FlightInfo]{
        var flights = [FlightInfo]()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        let invertTimeFormatter = DateFormatter()
        invertTimeFormatter.dateFormat = "yyyy-mm-dd'T'hh:mm:ss.SSS"
        
        for trip in trips{
            for tDate in trip.dates as! Set<TripDate>{
                for flight in tDate.flights as! Set<Flight>{
                    var fInfo = FlightInfo()
                    let time = flight.time as! Set<String>
                    let min: String = time.min()!
                    let max: String = time.max()!
                    let minTime: Date = invertTimeFormatter.date(from: min) ?? Date()
                    let maxTime: Date = invertTimeFormatter.date(from: max) ?? Date()
                    
                    fInfo.date = dateFormatter.string(from: tDate.dateOut!)
                    fInfo.time = timeFormatter.string(from: minTime) + " -> " + timeFormatter.string(from: maxTime)
                    fInfo.code = flight.flightNumber
                    fInfo.price = priceFormat(fare: flight.regularFare)
                    flights.append(fInfo)
                }
            }
        }
        return sortByDate(list: flights)
    }
    
    func priceFormat(fare: RBFare?) -> String{
        guard let fare = fare else{
            return String()
        }
        
        let adtFare = (fare.fares as! Set<Fare>).filter({$0.type == "ADT"})
        let amount = adtFare.first?.amount.description
        let currencySymbol = getSymbol(forCurrencyCode: self.currency)
        
        return amount! + " " + currencySymbol!
    }
    
    func setOrigin(code: String){
        searchDetail.origin = code
    }
    
    func setDestination(code: String){
        searchDetail.destination = code
    }
    
    func checkOrigin() -> Bool{
        !(searchDetail.origin?.isEmpty ?? false)
    }
    
    func checkDestination() -> Bool{
        !(searchDetail.destination?.isEmpty ?? false)
    }
    
    func clearDestination(){
        searchDetail.destination = String()
    }
    
    func clearOrigin(){
        searchDetail.origin = String()
    }
    
    func getFlightCount() -> Int{
        flights.count
    }
    
    func getFlightDate(for index: Int) -> String{
        (flights[index].date ?? "") + " " + (flights[index].time ?? "")
    }
    
    func getFlightCode(for index: Int) -> String{
        return flights[index].code ?? ""
    }
    
    func getFlightRegularFare(for index: Int) -> String{
        return flights[index].price ?? ""
    }
    
    func getSymbol(forCurrencyCode code: String) -> String? {
        let locale = NSLocale(localeIdentifier: code)
        if locale.displayName(forKey: .currencySymbol, value: code) == code {
            let newlocale = NSLocale(localeIdentifier: code.dropLast() + "_en")
            return newlocale.displayName(forKey: .currencySymbol, value: code)
        }
        return locale.displayName(forKey: .currencySymbol, value: code)
    }
}

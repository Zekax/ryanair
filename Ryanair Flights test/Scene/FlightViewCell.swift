//
//  FlightViewCell.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 18/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import UIKit

class FlightViewCell: UITableViewCell {
    
    lazy var containerView: UIView = {
        var view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var dateLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.text = "06:30"
        textLabel.font = UIFont(name: "HelveticaNeue", size: 14)
        textLabel.textAlignment = .left
        textLabel.textColor = .black
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
    
    lazy var flightLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.text = "RY2045"
        textLabel.font = UIFont(name: "HelveticaNeue", size: 14)
        textLabel.textAlignment = .left
        textLabel.numberOfLines = 1
        textLabel.textColor = .black
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
    
    lazy var valueLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.text = "120.00€"
        textLabel.font = UIFont(name: "HelveticaNeue", size: 14)
        textLabel.textAlignment = .right
        textLabel.numberOfLines = 1
        textLabel.textColor = .black
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
    
    private func setupUI() {
        addViews()
        addConstraint()
    }
    
    private func addViews() {
        backgroundColor = .white

        addSubview(containerView)

        containerView.addSubview(dateLabel)
        containerView.addSubview(flightLabel)
        containerView.addSubview(valueLabel)
    }
    
    private func addConstraint() {
        containerView.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(6)
            make.top.bottom.equalToSuperview().inset(6)
        }
        dateLabel.snp.makeConstraints { (make) in
            make.leading.trailing.equalToSuperview().inset(6)
            make.top.equalToSuperview().offset(6)
        }
        flightLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().inset(6)
            make.top.equalTo(dateLabel.snp.bottom).offset(6)
            make.bottom.equalToSuperview().inset(6)
        }
        valueLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(flightLabel.snp.trailing).inset(6)
            make.trailing.equalToSuperview().inset(6)
            make.top.equalTo(dateLabel).offset(6)
        }
    }
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

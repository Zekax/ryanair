//
//  PaddedTxtField.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 18/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation
import UIKit

class PaddedTxtField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init(placeholder: String){
        super.init(frame: CGRect.zero)
        self.placeholder = placeholder
        setup()
    }
    
    func setup(){
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        self.textColor = .black
        self.font = UIFont(name: "HelveticaNeue", size: 14)
        self.layer.cornerRadius = 4
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.gray.cgColor
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.textRect(forBounds: bounds)
        textRect.origin.x = 10
        textRect.size.width = rightViewRect(forBounds: bounds).origin.x - 10
        return textRect
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        var placeholderRect = super.placeholderRect(forBounds: bounds)
        placeholderRect.origin.x = 10
        placeholderRect.size.width = rightViewRect(forBounds: bounds).origin.x - 10
        return placeholderRect
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var editingRect = super.editingRect(forBounds: bounds)
        editingRect.origin.x = 10
        editingRect.size.width = rightViewRect(forBounds: bounds).origin.x - 10
        return editingRect
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var editingRect = super.rightViewRect(forBounds: bounds)
        editingRect.origin.x -= 10
        return editingRect
    }
}

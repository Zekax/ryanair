//
//  ServiceManager.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation
import RestKit

typealias rkCallbackFunc = (NSError?, Any?, Int?) -> ()



enum urlPath:String{
    case stations = "https://tripstest.ryanair.com/static/stations.json"
    case flights = "https://sit-nativeapps.ryanair.com/api/v4/Availability?origin=%@&destination=%@&dateout=%@&datein=&flexdaysbeforeout=3&flexdaysout=3&flexdaysbeforein=3&flexdaysin=3&adt=%@&teen=%@&chd=%@&roundtrip=false&ToUs=AGREED"
}

struct SearchDetail{
    var origin: String?
    var destination: String?
    var dateout: String?
    var adt: String?
    var teen: String?
    var chd: String?
}

class ServiceManager: NSObject{
    
    static func stationMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Station.self)
        mapping?.addAttributeMappings(from: ["name", "code"])
        return mapping!
    }
    
    static func availabilityMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Availability.self)
        mapping?.addAttributeMappings(from: ["currency"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "trips", toKeyPath: "trips", with: tripMapping()))
        return mapping!
    }
    
    static func tripMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Trip.self)
        mapping?.addAttributeMappings(from: ["origin", "destination"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "dates", toKeyPath: "dates", with: dateMapping()))
        return mapping!
    }
    
    static func dateMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: TripDate.self)
        mapping?.addAttributeMappings(from: ["dateOut"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "flights", toKeyPath: "flights", with: flightMapping()))
        return mapping!
    }
    
    static func flightMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Flight.self)
        mapping?.addAttributeMappings(from: ["time", "flightNumber"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "regularFare", toKeyPath: "regularFare", with: rbFareMapping()))
        return mapping!
    }
    
    static func rbFareMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: RBFare.self)
        mapping?.addAttributeMappings(from: ["fareKey", "fareClass"])
        mapping?.addPropertyMapping(RKRelationshipMapping(fromKeyPath: "fares", toKeyPath: "fares", with: fareMapping()))
        return mapping!
    }
    
    static func fareMapping() -> RKObjectMapping{
        let mapping = RKObjectMapping(for: Fare.self)
        mapping?.addAttributeMappings(from: ["amount", "count", "type", "hasDiscount", "publishedFare"])
        return mapping!
    }
       
    //MARK: - Requests
    static func requestStations(callback: @escaping rkCallbackFunc){
        let path = urlPath.stations.rawValue
        let resDescriptor = RKResponseDescriptor(mapping: stationMapping(), method: RKRequestMethod.GET, pathPattern:nil, keyPath: "stations", statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.successful))
        requestOperation(url: path, responseDescriptor: resDescriptor!, requestComplete: callback)
    }
    
    static func requestFlights(checkFor: SearchDetail, callback: @escaping rkCallbackFunc){
        let path = String(format: urlPath.flights.rawValue, checkFor.origin! , checkFor.destination!, checkFor.dateout!, checkFor.adt!, checkFor.teen!, checkFor.chd!)
        let resDescriptor = RKResponseDescriptor(mapping: availabilityMapping(), method: RKRequestMethod.GET, pathPattern:nil, keyPath: nil, statusCodes: RKStatusCodeIndexSetForClass(RKStatusCodeClass.successful))
        requestOperation(url: path, responseDescriptor: resDescriptor!, requestComplete: callback)
    }
    
    // MARK: - REQUEST
            
    /// Unmanaged object request, use when Core Data persistence is not needed.
    ///
    /// - Parameters:
    ///   - url: the url to make the request
    ///   - responseDescriptor: the response descriptor containing the object mapping and request method
    ///   - requestComplete: callback function to execute on request response
    static func requestOperation(url: String, responseDescriptor: RKResponseDescriptor, headerValues: [String:String]? = nil, requestComplete:rkCallbackFunc? = nil){
        
        var request = URLRequest(url: URL(string:url.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!)! )
        
        if headerValues != nil{
            for hv in headerValues!{
                request.setValue(hv.value, forHTTPHeaderField: hv.key)
            }
        }
        
        let operation = RKObjectRequestOperation(request: request, responseDescriptors: [responseDescriptor])
    
        operation?.setCompletionBlockWithSuccess({(moperation, mappingResult) -> Void in
            var result:Any?
            if mappingResult?.count ?? 0 > UInt(1){
                result = mappingResult?.array()
            }else{
                result = mappingResult?.firstObject
            }
            
            requestComplete?(nil, result, moperation?.httpRequestOperation.response.statusCode)
            moperation?.cancel()
        }, failure: { (moperation, error) -> Void in
            guard let nsError = error as NSError? else{
                return
            }
            requestComplete?(nsError, nil, moperation?.httpRequestOperation.response?.statusCode)
            moperation?.cancel()
        })
        let operationQueue = OperationQueue()
        operationQueue.addOperation(operation!)
    }
}

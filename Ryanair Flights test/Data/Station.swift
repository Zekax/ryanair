//
//  Station.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation

@objc(Station)
class Station: NSObject{
    @objc dynamic var name: String?
    @objc dynamic var code: String?
}

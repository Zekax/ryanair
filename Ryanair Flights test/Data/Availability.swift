//
//  Availability.swift
//  Ryanair Flights test
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import Foundation

@objc(Availability)
class Availability: NSObject{
    @objc dynamic var currency: String?
    @objc dynamic var trips: NSSet?
}

@objc(Trip)
class Trip: NSObject{
    @objc dynamic var origin: String?
    @objc dynamic var destination: String?
    @objc dynamic var dates: NSSet?
}

@objc(TripDate)
class TripDate: NSObject{
    @objc dynamic var dateOut: Date?
    @objc dynamic var flights: NSSet?
}

@objc(Flight)
class Flight: NSObject{
    @objc dynamic var time: NSSet?
    @objc dynamic var flightNumber: String?
    @objc dynamic var regularFare: RBFare?
    
}

@objc(RBFare)
class RBFare: NSObject{
    @objc dynamic var fareKey: String?
    @objc dynamic var fareClass: String?
    @objc dynamic var fares: NSSet?
    
}

@objc(Fare)
class Fare: NSObject{
    @objc dynamic var amount: Double = 0
    @objc dynamic var count: Int = 0
    @objc dynamic var type: String?
    @objc dynamic var hasDiscount: Bool = false
    @objc dynamic var publishedFare: Double = 0
    
}

//
//  Ryanair_Flights_testTests.swift
//  Ryanair Flights testTests
//
//  Created by Jose Rodrigues on 19/06/2020.
//  Copyright © 2020 Ryanair. All rights reserved.
//

import XCTest
import RestKit
@testable import Ryanair_Flights_test

class Ryanair_Flights_testTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        let testTargetBundle = Bundle(identifier: "com.ryanair.Ryanair-Flights-testTests")
        RKTestFixture.setFixtureBundle(testTargetBundle)
    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testStationMapping() {
        let parsedJson = RKTestFixture.parsedObject(withContentsOfFixture: "stations.json")
        let test = RKMappingTest(for: ServiceManager.stationMapping(), sourceObject: parsedJson, destinationObject: nil)
        test?.addExpectation(RKPropertyMappingTestExpectation(sourceKeyPath: "name", destinationKeyPath: "name", value:nil))
        
        XCTAssert(test!.evaluate)
    }
    
    func testFlightMapping() {
        let parsedJson = RKTestFixture.parsedObject(withContentsOfFixture: "flights.json")
        let test = RKMappingTest(for: ServiceManager.availabilityMapping(), sourceObject: parsedJson, destinationObject: nil)
        test?.addExpectation(RKPropertyMappingTestExpectation(sourceKeyPath: "trips", destinationKeyPath: "trips", value:nil))
        
        XCTAssert(test!.evaluate)
    }
    
    func testRequestStations(){
        let promise = expectation(description: "GET success")
        var statusCode: Int?
        var responseError: Error?
        ServiceManager.requestStations(callback: {(error, result, code) in
            responseError = error
            statusCode = code
            promise.fulfill()
        })
        wait(for: [promise], timeout: 25)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }
    
    func testRequestFlights(){
        let promise = expectation(description: "GET success")
        var statusCode: Int?
        var responseError: Error?
        let detail = SearchDetail(origin: "OPO", destination: "BCN", dateout: "2020-07-10", adt: "1", teen: "0", chd: "0")
        ServiceManager.requestFlights(checkFor: detail, callback: {(error, result, code) in
            responseError = error
            statusCode = code
            promise.fulfill()
        })
        wait(for: [promise], timeout: 25)
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
